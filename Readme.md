# Tugas 1 PPW Kelompok C Semester Genap 2017/2018 (kelas pak Gladhi)

Nama-nama Anggota Kelompok:
1. Tara Mecca Luna (fitur 3)
2. Muzakki H. Azmi (fitur 2)
3. Mohammad Raffi A. (fitur 1 dan 4)


Status Pipelines:
![pipeline status](https://gitlab.com/muzakkiha/tugas1ppw-c/badges/master/pipeline.svg)

Coverage Report:
![coverage report](https://gitlab.com/muzakkiha/tugas1ppw-c/badges/master/coverage.svg)


Link Herokuapp:
http://cteamm.herokuapp.com/