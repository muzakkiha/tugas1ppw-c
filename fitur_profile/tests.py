from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, profile_image_url, name, birthdate, gender, expertise, description, email

class ProfileUnitTest(TestCase):
	# Create your tests here.
	def test_fitur_profile_url_is_exist(self):
	    response = Client().get('/fitur-profile/')
	    self.assertEqual(response.status_code, 200)

	def test_fitur_profile_using_index_func(self):
	    found = resolve('/fitur-profile/')
	    self.assertEqual(found.func, index)

	def test_fitur_profile_data_written(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')

		self.assertIn(profile_image_url, html_response)
		self.assertTrue(len(profile_image_url) > 0)

		self.assertIn(name, html_response)
		self.assertTrue(len(name) > 0)

		self.assertIn(birthdate, html_response)
		self.assertTrue(len(birthdate) > 0)

		self.assertIn(gender, html_response)
		self.assertTrue(gender == "Male" or gender == "Female")

		for i in expertise:
			self.assertTrue(len(i) > 0)

		self.assertIn(email, html_response)
		self.assertTrue(len(email) > 0 and "@" in email)

		self.assertTrue(len(description) >= 6)