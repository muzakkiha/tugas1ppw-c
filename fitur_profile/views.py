from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
response = {'author': "Kelompok C"}
profile_image_url = 'static/image/spiderman.png'
name = 'Spider Man'
birthdate = 'October 22, 1962'
gender = 'Male'
expertise = {'one': 'Web-Shooting Ability', 'two': 'Skilled Photographer', 'three': 'Martial Arts Expert'}
description = 'Experienced as a photographer and superhero in several base of operations in US. Also have a side job as an actor, often plays the role of himself in movies.'
email = 'spiderman@gmail.com'

def index(request):
	html = 'fitur_profile/fitur_profile.html'
	response['profile_image_url'] = profile_image_url
	response['name'] = name
	response['birthdate'] = birthdate
	response['gender'] = gender
	response['expertise'] = expertise
	response['description'] = description
	response['email'] = email
	return render(request, html, response)
