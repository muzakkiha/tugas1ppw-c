from django.shortcuts import render
from .forms import addFriendForms
from .models import Teman
from django.http import HttpResponseRedirect

# Create your views here.
response = {'author' : "Kelompok C"}
def index(request):
    html = 'add_friends/add_friends.html'
    response['addFriendForms'] = addFriendForms
    teman = Teman.objects.all()
    response['teman'] = teman
    return render(request, html, response)

def add_friends_post(request):
    form = addFriendForms(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        teman = Teman(nama_teman=response['name'], url_teman=response['url'])
        teman.save()
        html ='add_friends/friends_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/fitur-friends/')
