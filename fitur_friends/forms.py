from django import forms
from .models import Teman

class addFriendForms(forms.Form):
        error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
        attrs = {
        'class': 'form-control'
    }
        name = forms.CharField(label='Name', required=True, max_length=30,widget=forms.TextInput(attrs=attrs))
        url = forms.URLField(label='Url Website',required=True,max_length=40,widget=forms.URLInput(attrs=attrs))
