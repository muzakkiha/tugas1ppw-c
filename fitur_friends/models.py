from django.db import models

class Teman(models.Model) :
	nama_teman = models.CharField(max_length=50)
	url_teman = models.CharField(max_length=50)
	tanggal_teman = models.DateTimeField(auto_now_add=True)
