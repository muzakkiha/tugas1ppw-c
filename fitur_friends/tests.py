from django.test import TestCase
from django.shortcuts import render
from .forms import addFriendForms
from .models import Teman
from .views import index,add_friends_post
from django.test import Client
from django.urls import resolve

class Add_friendsUnitTest(TestCase):
	def test_add_friends_url_is_exist(self):
		response = Client().get('/fitur-friends/')
		self.assertEqual(response.status_code,200)

	def test_add_friends_using_index_func(self):
		found = resolve('/fitur-friends/')
		self.assertEqual(found.func,index)

	def test_model_can_create_new_friend(self):
		new_activity = Teman.objects.create(nama_teman="shavira",url_teman="https://asikasikjos98.herokuapp.com/")
		counting_all_available_friend = Teman.objects.all().count()
		self.assertEqual(counting_all_available_friend,1)

	def test_form_friends_post_success_and_render_the_result(self):
		anonymous = 'Anonymous'
		linknya = 'http://google.com'
		response_post = Client().post('/fitur-friends/add_friends_post/', {'name': anonymous, 'url': linknya})
		self.assertEqual(response_post.status_code, 200)
		response = Client().get('/fitur-friends/')
		html_response = response.content.decode('utf8')
		self.assertIn(anonymous,html_response)
		self.assertIn(linknya,html_response)
