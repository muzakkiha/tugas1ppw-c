from django.conf.urls import url
from .views import index, add_friends_post

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_friends_post/$', add_friends_post, name='add_friends_post'),
    ]
