"""tugas1ppwc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import fitur_statistic.urls as fitur_statistic
import fitur_status.urls as fitur_status
import fitur_profile.urls as fitur_profile
import fitur_friends.urls as fitur_friends
from fitur_status.views import index as index_fitur_status

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^fitur-status/', include(fitur_status, namespace='fitur-status')),
    url(r'^fitur-profile/', include(fitur_profile, namespace='fitur-profile')),
    url(r'^fitur-friends/', include(fitur_friends, namespace='fitur-friends')),
    url(r'^fitur-statistic/', include(fitur_statistic, namespace='fitur-statistic')),
    url(r'^$', index_fitur_status, name='index')
]
