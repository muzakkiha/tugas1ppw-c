from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
from fitur_profile.views import name as name11

# Create your views here.
response = {'author': "Kelompok C"}
name = name11
def index(request):
    html = 'fitur_status/fitur_status.html'
    response['name'] = name
    response['message_form'] = Message_Form
    message = Message.objects.all()
    response['message'] = message
    return render(request, html, response)

def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        message = Message(message=response['message'])
        message.save()
        html ='fitur_status/form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/fitur-status/')
