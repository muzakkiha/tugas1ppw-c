from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }

    message = forms.CharField(label='Apa yang sedang kamu pikirkan?', widget=forms.TextInput(attrs=attrs), required=True, max_length=140)
