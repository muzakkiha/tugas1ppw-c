from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, message_post
from .models import Message
from .forms import Message_Form
# Create your tests here.

class UpdateStatusUnitTest(TestCase):
    def test_update_status_url_is_exist(self):
        response = Client().get('/fitur-status/')
        self.assertEqual(response.status_code, 200)

    def test_update_status_using_index_func(self):
        found = resolve('/fitur-status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_message(self):
        #Creating a new activity
        new_activity = Message.objects.create(message='This is a test')

        #Retrieving all available activity
        counting_all_available_message= Message.objects.all().count()
        self.assertEqual(counting_all_available_message,1)

    def test_form_message_input_has_placeholder_and_css_classes(self):
        form = Message_Form()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('<label for="id_message">Apa yang sedang kamu pikirkan?</label>', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Message_Form(data={'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )

    def test_update_status_post_fail(self):
        response = Client().post('/fitur-status/add_message', {'message': ''})
        self.assertEqual(response.status_code, 302)

    def test_update_status_post_success_and_render_the_result(self):
        message = 'Helloo'
        response = Client().post('/fitur-status/add_message', {'message': message})
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn(message,html_response)

    def test_update_status_showing_all_messages(self):

        message_budi = 'Lanjutkan Kawan'
        data_budi = {'message': message_budi}
        post_data_budi = Client().post('/fitur-status/add_message', data_budi)
        self.assertEqual(post_data_budi.status_code, 200)

        response = Client().get('/fitur-status/')
        html_response = response.content.decode('utf8')

        for key,data in data_budi.items():
            self.assertIn(data,html_response)
