from django.db import models

class Message(models.Model):
    message = models.TextField(max_length=140)
    created_date = models.DateTimeField(auto_now_add=True)