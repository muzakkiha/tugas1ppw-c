from django.apps import AppConfig


class UpdateStatusConfig(AppConfig):
    name = 'fitur_status'
