from django.apps import AppConfig


class FiturStatisticConfig(AppConfig):
    name = 'fitur_statistic'
