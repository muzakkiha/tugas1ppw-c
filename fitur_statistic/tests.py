from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views  import index
from fitur_status.models import Message
#from add_friends import Teman
# Create your tests here.
class StatisticUnitTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response = Client().get('/fitur-statistic/')
        self.assertEqual(response.status_code,200)

    def test_if_header_is_exist(self):
        response = Client().get('/fitur-statistic/')
        html_response = response.content.decode('utf8')
        self.assertIn("Statistic",html_response)

    def test_if_footer_is_exist(self):
        response = Client().get('/fitur-statistic/')
        html_response = response.content.decode('utf8')
        self.assertIn("Kelompok C",html_response)

    def test_counting_the_feed(self):
        #Creating a new activity
        new_activity = Message.objects.create(message='Hellooo')

        #Retrieving all available activity
        counting_all_available_message= Message.objects.all().count()
        self.assertEqual(counting_all_available_message,1)
