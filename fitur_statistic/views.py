from django.shortcuts import render
from fitur_status.models import Message
from fitur_profile.views import name as name11
from fitur_friends.models import Teman

# Create your views here.
response = {'author': "Kelompok C"}
def index(request):
    html = 'fitur_statistic/fitur_statistic.html'
    message = Message.objects.all()
    teman = Teman.objects.all()
    response['name'] = name11
    total_message = message.count()
    total_friends = teman.count()
    response['message'] = message
    response['total_friends'] = total_friends

    if (total_message == 0):
        last_message = ""
        response['message'] = last_message
    else:
        last_message = message[message.count() - 1]
        response['message'] = last_message.message
        response['message_date'] = last_message.created_date


    response['total_message'] = total_message

    return render(request, html, response)
